-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2016 at 12:45 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expman_expdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE IF NOT EXISTS `balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbalance` int(11) NOT NULL,
  `iid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iid` (`iid`),
  KEY `eid` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pname` varchar(2000) NOT NULL,
  `pprice` float NOT NULL,
  `uid` int(3) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `uid_2` (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `pname`, `pprice`, `uid`, `date`) VALUES
(1, 'dal', 20, 1, '2016-08-12'),
(2, 'dal', 10, 1, '2016-08-11'),
(3, 'kola', 14, 1, '2016-08-11'),
(4, 'dal', 140, 7, '1900-01-01'),
(5, 'dal', 100, 7, '2016-08-17'),
(6, 'chal', 200, 7, '2016-08-16'),
(7, 'Electricity Bill', 1200, 8, '2016-08-18'),
(8, 'Water Bill', 600, 8, '2016-08-18'),
(9, 'Internet Bill', 800, 8, '1900-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE IF NOT EXISTS `income` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `income` varchar(2000) NOT NULL,
  `tvalue` float NOT NULL,
  `uid` int(3) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`id`, `income`, `tvalue`, `uid`, `date`) VALUES
(1, 'chal', 30, 1, '1900-01-01'),
(2, 'dal', 500, 1, '2016-08-11'),
(3, 'Chicken', 200, 7, '2016-08-16'),
(4, 'Gas', 1000, 7, '2016-08-17'),
(5, 'Salary', 10000, 8, '1900-01-01'),
(6, 'Rent', 8000, 8, '2016-08-17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(8) NOT NULL AUTO_INCREMENT,
  `uname` varchar(40) NOT NULL,
  `uemail` varchar(80) NOT NULL,
  `upass` varchar(32) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `uname`, `uemail`, `upass`) VALUES
(1, 'ayon', 'saief_antar@yahoo.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(2, 'ahmed saheb', 'sssaief_antar@yahoo.com', '26f3808cdec7fb1ba79cd549cba6e200'),
(3, 'ahmed', 'saieff_antar@yahoo.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(4, '12132', 'saieff_a1ntar@yahoo.com', 'c20ad4d76fe97759aa27a0c99bff6710'),
(5, '32342', 'saieff_antar@dsfds.om', '81dc9bdb52d04dc20036dbd8313ed055'),
(6, 'ayon', 'mirelahi@gmail.com', '35309226eb45ec366ca86a4329a2b7c3'),
(7, 'ayon', 'mirelahi33@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(8, 'Saief Sadat Khan', 'saief.antar@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
